// Generated using webpack-cli https://github.com/webpack/webpack-cli

const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const WorkboxWebpackPlugin = require("workbox-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");

const isProduction = process.env.NODE_ENV == "production";

const stylesHandler = isProduction
	? MiniCssExtractPlugin.loader
	: "style-loader";

const config = {
	entry: "./src/main.js",
	output: {
		path: path.resolve(__dirname, "build"),
	},
	devServer: {
		open: true,
		host: "localhost",
		proxy: {
			"/subscribe": "http://localhost:3000",
			"/unsubscribe": "http://localhost:3000",
			"/community": "http://localhost:3000",
			"analytics/performance": "http://localhost:3000",
			"/analytics/user": "http://localhost:3000",
		},
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: "./src/index.html",
		}),

		new CopyPlugin({
			patterns: [{ from: "./src/assets/images", to: "assets" }],
		}),

		new MiniCssExtractPlugin(),
		// Add your plugins here
		// Learn more about plugins from https://webpack.js.org/configuration/plugins/
	],
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/i,
				loader: "babel-loader",
				exclude: /node_modules/,
			},
			{
				test: /\\.css$/,
				sideEffects: true,
				use: [stylesHandler, "css-loader", "postcss-loader"],
			},
			{
				test: /\.(eot|svg|ttf|woff|woff2|png|jpg|gif)$/i,
				type: "asset",
				use: [
					{
						loader: "file-loader",
						options: {
							limit: 8192,
						},
					},
				],
			},
			{
				test: /\.html$/i,
				loader: "html-loader",
			},

			// Add your rules for custom modules here
			// Learn more about loaders from https://webpack.js.org/loaders/
		],
	},
};

module.exports = () => {
	if (isProduction) {
		config.mode = "production";

		config.plugins.push(new WorkboxWebpackPlugin.GenerateSW());
	} else {
		config.mode = "development";
	}
	return config;
};
