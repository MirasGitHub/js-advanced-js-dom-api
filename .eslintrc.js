module.exports = {
  env: {
    browser: true,
    es2021: true
  },
  extends: 'standard',
  overrides: [
    {
      env: {
        node: true
      },
      files: [
        '.eslintrc.{js,cjs}'
      ],
      parserOptions: {
        sourceType: 'script'
      }
    }
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module'
  },
  rules: {
    semi: [2, 'always'],
    'no-unused-vars': 'off',
    quotes: 'off',
    eqeqeq: 'off',
    "no-unused-expressions": 'off',
    'import/newline-after-import': 'error',
    indent: ['error', 2],
    'import/prefer-default-export': 'off',
    'keyword-spacing': 'error',
    'space-before-blocks': 'off',
    'eol-last': 'off'
  }
};
