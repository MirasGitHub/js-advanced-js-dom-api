const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const cors = require("cors"); // Import cors middleware

const indexRouter = require("./routes/index");
const communityRouter = require("./routes/community");
const analyticsRouter = require("./routes/analytics");

const app = express();
global.appRoot = path.resolve(__dirname);

app.use(logger("dev"));
// app.use(
// 	cors({
// 		origin: "http://127.0.0.1:5500",
// 		methods: ["GET", "POST"],
// 		allowedHeaders: ["Content-Type", "Authorization"],
// 		credentials: true,
// 	})
// );
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/community", communityRouter);
app.use("/analytics", analyticsRouter);

// app.post("/subscribe", (req, res) => {
// 	const { email } = req.body;

// 	res.json({ message: "Successfully subscribed!", email });
// });

module.exports = app;
