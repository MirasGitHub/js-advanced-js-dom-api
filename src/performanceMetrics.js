export function measurePerformance() {
	const startTime = performance.now();

	const performanceEntries = performance.getEntriesByType("navigation");

	performanceEntries.forEach((entry) => {
		console.log(
			`%cDomComplete -  %c${entry.domComplete.toFixed(2)} ms`,
			"color: darkblue",
			"color: green"
		);
		console.log(
			`%cDomInteractive -  %c${entry.domInteractive.toFixed(2)} ms`,
			"color: darkblue",
			"color: green"
		);
	});

	const loadTime = startTime - performance.timeOrigin;
	console.log(
		`%cPage load time: %c${loadTime.toFixed(2)} ms`,
		"color: purple",
		"color: tomato"
	);

	const memoryUsage = (
		performance.memory.usedJSHeapSize /
		(1024 * 1024)
	).toFixed(2);
	console.log(
		`%cMemory usage: %c${memoryUsage} MB`,
		"color: red",
		"color: green"
	);

	const metrics = [
		{
			metricName: "pageLoadTime",
			value: loadTime.toFixed(2),
		},
		{
			metricName: "memoryUsage",
			value: `${memoryUsage} MB`,
		},
	];

	// sendMetricsToServer(metrics);
}

export function measureFetchTime() {
	performance.mark("fetchCallEnd");

	const fetchCallMeasure = performance.measure(
		`fetchCall`,
		"fetchCallStart",
		"fetchCallEnd"
	);

	console.log(
		`%cFetch time - %c${fetchCallMeasure.duration.toFixed(2)} ms`,
		"color: brown",
		"color: green"
	);
}

export async function sendMetricsToServer(metrics) {
	const analyticsEndpoint = "http://localhost:3000/analytics/performance";

	try {
		const response = await fetch(analyticsEndpoint, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({ metrics }),
		});

		if (response.ok) {
			window.alert("Performance metrics sent successfully.");
			console.log("Performance metrics sent successfully.");
		} else {
			window.alert("Failed to send performance metrics.");
			console.error("Failed to send performance metrics.");
		}
	} catch (error) {
		window.alert("Failed to send metrics");
		console.error("Error sending performance metrics:", error);
	}
}
