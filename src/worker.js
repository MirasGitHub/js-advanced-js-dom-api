let events = [];
let clickCallsCount = 0;

self.onmessage = function (event) {
	const eventData = event.data;

	// console.log("message from the main thread - ", eventData);

	events.push(eventData);
	clickCallsCount++;

	if (events.length === 5) {
		sendEventsToServer();
	}

	async function sendEventsToServer() {
		try {
			const response = await fetch("http://localhost:3000/analytics/user", {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify({ events }),
			});
			if (response.ok) {
				console.log("The events data sent successfully");
				self.postMessage("The events data sent successfully");
				events = [];
			} else {
				console.log("Couldn't send events");
				self.postMessage("Couldn't send events");
			}
		} catch (err) {
			console.error("Failed to send events");

			self.postMessage("Couldn't send events");
		}
	}

	switch (eventData.action) {
		case "subscribe":
			self.postMessage(eventData);
			break;
		case "unSubscribe":
			self.postMessage(eventData);
			break;
		case "inputClick":
			self.postMessage(eventData);
			break;
		case "readMore":
			self.postMessage(eventData);
			break;
		case "videoClick":
			self.postMessage(eventData);
			break;
	}
};
