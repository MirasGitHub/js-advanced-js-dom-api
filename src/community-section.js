import {
	measurePerformance,
	measureFetchTime,
	sendMetricsToServer,
} from "./performanceMetrics.js";

window.addEventListener("load", () => {
	measurePerformance();
});

performance.mark("fetchCallStart");

export async function fetchCommunityData() {
	const endpoint = "http://localhost:3000/community";

	try {
		const response = await fetch(endpoint);
		const data = await response.json();

		displayCommunityData(data);

		measureFetchTime();
	} catch (error) {
		console.error("Error fetching community data:", error);
	}
}

function displayCommunityData(data) {
	const communityMembersContainer =
		document.querySelector(".community-members");

	data?.forEach(({ avatar, firstName, lastName, position }) => {
		const memberDiv = document.createElement("div");
		memberDiv.className = "member";
		memberDiv.innerHTML = `
            <img src="${avatar}" alt="${avatar}" width="${120}" height="${120}">
            <h3>${firstName} ${lastName}</h3>
            <p>${position}</p>
        `;
		communityMembersContainer.appendChild(memberDiv);
	});
}
