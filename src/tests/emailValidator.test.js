import { JSDOM } from "jsdom";
import {
	validate,
	validateAsync,
	validateWithThrow,
	validateWithLog,
} from "../email-validator";
import chai from "chai";
import { expect } from "chai";
import sinon from "sinon";

const assert = chai.assert;

const VALID_EMAILS = [
	"example@gmail.com",
	"test@outlook.com",
	"user@yandex.ru",
];

const INVALID_EMAILS = [
	"example@gmail.org",
	"test@something.com",
	"user@invalid.ru",
];

const EMAILS_WITH_SPACES = [
	"exa mple@gmail.org",
	"test@some thing.com",
	"user@ invalid.ru",
];

let email;

beforeEach(() => {
	email = "user@gmail.com";
});

describe("validate", () => {
	it("should return true if the email has more than 14 characters", () => {
		expect(validate(email)).to.be.true;
	});

	it("should return false if the email has less than 14 characters", () => {
		email = "hi@gmail.com";
		expect(validate(email)).to.be.false;
	});

	it("should return false if the email has now been passed to validate function", () => {
		expect(validate()).to.be.false;
	});

	it("should return false if the email contains spaces", () => {
		for (let email of EMAILS_WITH_SPACES) {
			expect(validate(email)).to.be.false;
		}
	});

	describe("validateAsync", () => {
		let document;
		before(() => {
			const dom = new JSDOM("<!DOCTYPE html><html><body></body></html>");
			document = dom.window.document;
		});

		it("should return true asynchronously if the email contains a valid ending", async () => {
			for (let email of VALID_EMAILS) {
				const result = await validateAsync(email);
				expect(result).to.equal(true);
			}
		});

		it("should return false asynchronously for the invalid email endings", async () => {
			for (let email of INVALID_EMAILS) {
				const result = await validateAsync(email);
				expect(result).to.equal(false);
			}
		});
	});

	describe("validateWithThrow", () => {
		it("should return true for valid email endings", () => {
			for (let email of VALID_EMAILS) {
				expect(validateWithThrow(email)).to.equal(true);
			}
		});

		it("should throw an error for valid email endings with the message that the email is invalid", () => {
			const errorMessage = "Provided email is invalid!";

			for (let email of INVALID_EMAILS) {
				assert.throws(
					() => {
						validateWithThrow(email);
					},
					Error,
					errorMessage
				);
			}
		});
	});

	describe("validateWithLog", () => {
		let consoleStub;

		beforeEach(() => {
			consoleStub = sinon.stub(console, "log");
		});

		afterEach(() => {
			consoleStub.restore();
		});

		it("should log the result to the console before returning it", () => {
			const VALID_EMAILS = [
				"example@gmail.com",
				"test@outlook.com",
				"user@yandex.ru",
			];

			for (let email of VALID_EMAILS) {
				validateWithLog(email);
				expect(consoleStub.calledWith("Email - ", email)).to.be.true;
			}
		});
	});
});
