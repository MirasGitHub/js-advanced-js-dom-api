const VALID_EMAIL_ENDINGS = ["gmail.com", "outlook.com", "yandex.ru"];

export function validate(email) {
	let hasSpaces = /\s/.test(email);
	if (hasSpaces) {
		return false;
	} else {
		return !!email && email.length >= 14;
	}
}

export async function validateAsync(email) {
	const emailEnding = email.split("@")[1];
	if (VALID_EMAIL_ENDINGS.includes(emailEnding)) {
		return true;
	} else {
		return false;
	}
}

export function validateWithThrow(email) {
	const emailEnding = email.split("@")[1];
	if (VALID_EMAIL_ENDINGS.includes(emailEnding)) {
		return true;
	} else {
		throw new Error("Provided email is invalid!");
	}
}

export function validateWithLog(email) {
	console.log("Email - ", email);
	return true;
}
