import { createStandardSection } from "./join-us-section.js";

import { WebsiteSection } from "./WebsiteSection.js";

import { fetchCommunityData } from "./community-section.js";

const programSection = createStandardSection();

customElements.define("website-section", WebsiteSection);

window.addEventListener("load", async () => {
	await fetchCommunityData();
});
