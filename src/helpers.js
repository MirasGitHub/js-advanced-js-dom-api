import {
	validate,
	removeSubscriptionField,
	currentValue,
	unSubscribe,
} from "./email-validation.js";

export const section = document.getElementById("join-our-program");

export const btn = document.createElement("input");
export let subscribeMessage = document.createElement("h3");

btn.value = "Subscribe";

export const input = document.createElement("input");
input.type = "text";
input.placeholder = "Email";

input.addEventListener("input", (e) => {
	input.value = e.target.value;

	localStorage.setItem("Email", e.target.value);
});

export const displayErrorMessage = () => {
	localStorage.removeItem("Email");
	subscribeMessage.remove();
	input.style.border = "1px solid red";
	input.placeholder = "Please enter a valid email";
};

btn.type = "submit";
btn.className = "subscribe-btn";

export const form = document.createElement("form");
form.className = "subscription-container";
form.style.flexDirection = "row";

form.addEventListener("submit", subscribe);

subscribeMessage.innerHTML = "Thanks for Subscribing!";
subscribeMessage.style.color = "lightgreen";

export let initialCondition = localStorage.key(1) === "Email" ? true : false;

let isSubscribed = localStorage.getItem("isSubscribed") === "true";

function subscribe(e) {
	e.preventDefault();

	if (currentValue === false || currentValue === undefined) {
		validate();
	} else {
		unSubscribe();
	}
}

export function displaySubscriptionField() {
	if (isSubscribed) {
		input.className = "successful";
		btn.value = "UnSubscribe";
		subscribeMessage.innerHTML = "You subscribed our program.";

		form.style.flexDirection = "column";

		form.append(btn);

		btn.removeEventListener("submit", subscribe);
		btn.addEventListener("click", removeSubscriptionField);
		form.append(subscribeMessage);
	} else {
		subscribeMessage.style.display = "none";
		input.className = "subscribe-input";
		btn.value = "Subscribe";

		form.append(input);
		form.append(btn);

		form.removeEventListener("click", removeSubscriptionField);
		form.addEventListener("submit", subscribe);

		section.append(form);
	}
}
