import { input, btn } from "./helpers.js";

const worker = new Worker("worker.js");

let clickCallsCount = 0;

export function sendMessagesToWorker() {
	input.addEventListener("click", (event) => {
		clickCallsCount++;
		worker.postMessage({
			action: "inputClick",
			message: "Input field click",
			clickCallsCount,
		});
	});

	const videoBtn = document.querySelector(".app-section__button--our-culture");

	videoBtn.addEventListener("click", (event) => {
		clickCallsCount++;
		worker.postMessage({
			action: "videoClick",
			message: "Video btn click",
			clickCallsCount,
		});
	});

	const readMoreButton = document.querySelector(
		".app-section__button--read-more"
	);

	readMoreButton.addEventListener("click", (event) => {
		clickCallsCount++;
		worker.postMessage({
			action: "readMore",
			message: "Read More btn click",
			clickCallsCount,
		});
	});

	btn.addEventListener("click", (event) => {
		clickCallsCount++;
		worker.postMessage({
			action: "subscribe",
			message: input.value ? input.value : "no Email provided",
			clickCallsCount,
		});
	});

	worker.addEventListener("message", (event) => {
		if (event.data.clickCallsCount === 5) {
			window.alert("Successfully sent events data to server!");
		}
		// console.log("message from worker - ", event.data);
	});
}
