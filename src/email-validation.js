import {
	btn,
	input,
	form,
	displayErrorMessage,
	subscribeMessage,
	section,
} from "./helpers.js";

export let currentValue;

const VALID_EMAIL_ENDINGS = ["gmail.com", "outlook.com", "yandex.ru"];

const worker = new Worker(`worker.js`);

export async function unSubscribe() {
	const message = "Successfully unsubscribed";
	const emailFromLocalStorage = localStorage.getItem("Email");

	worker.postMessage({ action: "unSubscribe", message: emailFromLocalStorage });

	try {
		btn.disabled = true;
		btn.style.opacity = "0.5";

		const response = await fetch("http://localhost:3000/unsubscribe", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({ message }),
		});

		if (response.ok) {
			localStorage.removeItem("Email");

			window.alert("Successfully unsubscribed!");
		} else {
			console.log(response);
			throw new Error("Failed to unsubscribe. Please try again later.");
		}
	} catch (error) {
		console.error("Error unsubscribing:", error);
	}

	form.prepend(input);
	input.className = "subscribe-input";
	input.value = "";
	input.style.border = "";
	input.placeholder = "Email";
	btn.value = "Subscribe";
	localStorage.removeItem("Email");
	currentValue = false;
	localStorage.setItem("isSubscribed", false);
	subscribeMessage.style.display = "none";
	form.style.flexDirection = "row";
	btn.removeEventListener("click", removeSubscriptionField);
	btn.disabled = false;
	btn.style.opacity = "1";
}

export function removeSubscriptionField(e) {
	e.preventDefault();
	unSubscribe();
}

export async function validate() {
	const emailEnding = input.value.split("@")[1];

	if (
		VALID_EMAIL_ENDINGS.includes(emailEnding) &&
		input.value !== "forbidden@gmail.com"
	) {
		form.removeEventListener("click", removeSubscriptionField);

		currentValue = JSON.parse(localStorage.getItem("isSubscribed"));

		currentValue = true;

		localStorage.setItem("isSubscribed", currentValue);

		input.className = "successful";

		let email = input.value;

		btn.value = "Unsubscribe";
		subscribeMessage.style.display = "block";
		form.style.flexDirection = "row";

		section.append(subscribeMessage);

		try {
			// btn.disabled = true;
			// btn.style.opacity = "0.5";
			const response = await fetch("http://localhost:3000/subscribe", {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify({ email }),
			});

			if (response.ok) {
				input.className = "successful";
				btn.value = "Unsubscribe";
				subscribeMessage.style.display = "block";
				form.style.flexDirection = "row";
				section.append(subscribeMessage);
				btn.disabled = false;
				btn.style.opacity = "1";

				window.alert("Successful subscription!");
			} else {
				console.log(response);
				throw new Error("Failed to subscribe. Please try again later.");
			}
		} catch (error) {
			console.error("Error subscribing:", error);
		}
	} else if (input.value === "forbidden@gmail.com") {
		btn.disabled = true;
		btn.style.opacity = "0.5";

		try {
			const response = await fetch("http://localhost:3000/subscribe", {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify({ email: input.value }),
			});
			if (response.status === 422 && input.value === "forbidden@gmail.com") {
				btn.disabled = false;
				btn.style.opacity = "1";

				localStorage.removeItem("Email");
				currentValue = false;
				displayErrorMessage();
				window.alert("Forbidden Email Address!");
			}
		} catch (error) {
			btn.disabled = false;
			btn.style.opacity = "1";

			console.error("Error subscribing:", error);
		}
		btn.disabled = false;
		btn.style.opacity = "1";
	} else {
		btn.disabled = false;
		btn.style.opacity = "1";
		displayErrorMessage();
	}
}

worker.onmessage = function (event) {
	console.log("Message from Worker-", event);
};

// worker.addEventListener("message", (event) => {
// 	console.log("Message from worker", event);
// });
