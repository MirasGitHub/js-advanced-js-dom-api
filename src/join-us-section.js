import { form, displaySubscriptionField, section } from "./helpers.js";

import { sendMessagesToWorker } from "./sendMessagesToWorker.js";

function SectionCreator(title, content) {
	const elements = `<h2 class='join-our-program--h2'>${title}</h2>
    <p class='join-our-program--p'>${content}</p>`;

	displaySubscriptionField();

	sendMessagesToWorker();

	section.innerHTML = elements;
	section.append(form);

	return {
		remove: function () {
			section.style.display = "none";
		},
	};
}

export function createStandardSection() {
	return new SectionCreator(
		"Join our Program",
		"Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
	);
}
